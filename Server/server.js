require(__dirname + '/Resources/config.js');
var fs = require('fs')
var net = require('net')
require('./packet.js')

//Initialize the Server
var init_files = fs.readdirSync(__dirname + "/Initializers");
init_files.forEach(function(initfile){
    console.log("Loading Initializer: " + initfile)
    require(__dirname + "/Initializers/" + initfile)
})


var models_files = fs.readdirSync(__dirname + "/Models");
models_files.forEach(function(model){
    console.log("Loading Model: " + model)
    require(__dirname + "/Models/" + model)
})

maps = {};
var maps_files = fs.readdirSync(config.data_paths.maps);
maps_files.forEach(function(mapfile){
    var map = require(config.data_paths.maps + mapfile)
    maps[map.room] = map;
    console.log("Loading Map: " + map.name)
})


clients = [];

//Handle incoming sockets
net.createServer(function(socket){

    console.log('socket connected');
    var cinst = new require('./client');
    var thisClient = new cinst();
    thisClient.socket = socket;
    thisClient.initiate();
    clients.push(thisClient);


    socket.on('error', function(err) {
        thisClient.error(err);
    })

    socket.on('data', function (data) {
        thisClient.data(data);
    });

    socket.on('end', function () {
        thisClient.end()
    });


}).listen(config.port);

console.log("Initialize Completed, Server Running on PORT: " + config.port + " for ENVIRONMENT: " + config.environment);
