/**
 * Created by rm2kdev on 22/11/2014.
 */
///Initialize
var now = require("performance-now");
var _ = require("underscore");

module.exports = function (){

    this.socket = {};
    this.user = {};

    this.initiate = function(){
        var client = this;
        //Send the connection confirmation
        client.socket.write(packet.build(["HELLO", now().toString()]));

        //Setup a heartbeat so the client can visually confirm connection
        client.heartbeat = setInterval(function(){
            client.socket.write(packet.build(["HEARTBEAT", now().toString()]))
        }, 1500)

    }

    this.data = function(data){
        var client = this;
        packet.parse(client, data);
    }

    this.error = function(err){
        var client = this;
        console.log(err);
    }

    this.end = function(){
        var client = this;
        //stop the heartbeats
        clearInterval(client.heartbeat);

        //if the user was logged in
        if(client.user){
            //remove the user from whatever room they were in
            //maps[client.user.current_room].clients.splice(maps[client.user.current_room].clients.indexOf(client));
            client.exitroom(client.user.current_room);
        }

        //remove the client from the global client list
        clients.splice(clients.indexOf(client), 1);

        //Log the event
        console.log('socket closed')
    }

    this.enterroom = function(selected_room){
        var client = this;

        //notify everyone that the current client has entered the room.
        //add the player to that room.
    //    maps[selected_room].clients.forEach(function(otherClient){
    //        otherClient.socket.write(packet.build(['ENTER', client.user.username]))
    //    })

        maps[selected_room].clients.push(client);

    }

    this.exitroom =function(selected_room){
        var client = this;

        //remove the current client from the room and notify everyone that the player has left.
        console.log("Exit Room: " + client.user.username + " leaves the room " + selected_room);

        maps[selected_room].clients = _.reject(maps[selected_room].clients, function(c) {return c.user.username == client.user.username});

    },

    this.broadcastroom = function(packetData){
        var client = this;

        maps[client.user.current_room].clients.forEach(function(otherClient){
            if(otherClient.user.username != client.user.username){
                otherClient.socket.write(packetData);
            }
        })

    }

};
//var client = {};
//client.user - this is added on after the user logs in
//client.socket - this is added when the user connects to the server for the first time

//Log socket errors

//Remove and cleanup the client when the socket closes



///Game Helpers




//Broadcasts a packet to the current room, excludes this client




