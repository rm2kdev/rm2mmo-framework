/**
 * Created by rm2kdev on 21/11/2014.
 */
var args = require('minimist')(process.argv.slice(2));
var extend = require('extend')

//Store the environment variable
var environment = args.env || "test";

//Common config settings ie: name, version, max players etc
var common_conf = {
    name : "Rm2kdevs Game Server",
    version: "0.0.1",
    environment: environment,
    max_players: 10,
    data_paths: {
        items: __dirname + "\\Game Data\\" + "Items\\",
        maps: __dirname + "\\Game Data\\" + "Maps\\"
    },
    starting_zone: "rm_map_home"
}

//Environment specific config ie IP, Port, Datapaths etc.
var conf = {
    production :{
        ip: args.ip || "0.0.0.0",
        port: args.port || 8081,
        database: "mongodb://localhost:27017/rm2mmo" //replace ths with the live db
    },

    test: {
        ip: args.ip || "127.0.0.1",
        port: args.port || 8082,
        database: "mongodb://localhost:27017/rm2mmo"
    }
}

//Extend the specific environment configs with the common configs
extend(false, conf.production, common_conf);
extend(false, conf.test, common_conf);

//Export the config
module.exports = config = conf[environment];
