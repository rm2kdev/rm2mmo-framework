//argument0: data buffer
var command = buffer_read(argument0, buffer_string);
show_debug_message("Network Event: " + command)
switch(command){

    case "HELLO":
        server_time = buffer_read(argument0, buffer_string);
        room_goto_next();
        show_debug_message("Server Welcomes You @ " + server_time);
        break;
        
    case "HEARTBEAT":
        server_time = buffer_read(argument0, buffer_string);
        obj_Network_HeartBeat.image_alpha = 1;
        show_debug_message("Heart Beats @ " + string(server_time));
        break;
        
    case "REGISTER":
        status = buffer_read(argument0, buffer_string);
        if(status == "TRUE"){
            show_message("Register Success: Please login.")
        }else{
            show_message("Registration Failed: Username taken.")
        }
        show_debug_message("Register: " + status);
        break;
        
    case "LOGIN":
        status = buffer_read(argument0, buffer_string);
        if(status == "TRUE"){
            targetroom = buffer_read(argument0, buffer_string);
            target_x = buffer_read(argument0, buffer_u16);
            target_y = buffer_read(argument0, buffer_u16);
            goto_room = asset_get_index(targetroom)
            name = buffer_read(argument0, buffer_string);
            room_goto(goto_room);
            with(instance_create(target_x, target_y, obj_Player)){
                name = other.name
            }
        }else{
            show_message("login Failed")
        }
        show_debug_message("Login: " + status);
        break;   
        
    case "POS":
        username = buffer_read(argument0, buffer_string);
        target_x = buffer_read(argument0, buffer_u16);
        target_y = buffer_read(argument0, buffer_u16);
        
        foundPlayer = -1;
        with(obj_Network_Player){
            show_debug_message("TRAVERSE: " + name + " | "  + other.username)
            if(name == other.username){
                other.foundPlayer = id;
                break;
            }
        }
        if(foundPlayer != -1){
            with(foundPlayer){
                target_x = other.target_x;
                target_y = other.target_y;
            }
        }else{
            with(instance_create(target_x, target_y, obj_Network_Player)){
                show_debug_message("created player " + other.username)
                name = other.username;
            }
        }
        
        show_debug_message("USER: " + string(username) + " Moves To: " + string(target_x) + ", " + string(target_y))

        break;
}
